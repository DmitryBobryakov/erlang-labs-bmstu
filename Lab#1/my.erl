-module(my).
-export([start/0]).
-import(io, [fwrite/1]).

start() ->
	Res=fib:fib_p(10),
	io:format("~p", [Res]),
 	
	io:format("~n"),

	Res1=fib:fib_when(10),
	io:format("~p", [Res1]),

	io:format("~n"),

	Res2=fib:tail_fib(10),
	io:format("~p", [Res2]). 
 	
