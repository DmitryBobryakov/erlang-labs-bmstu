-module(fib).

-export([
    fib_p/1,
    fib_when/1,
    tail_fib/1
]).

fib_p(0) ->0;
fib_p(1) ->1;
fib_p(N) ->fib_p(N - 1) + fib_p(N - 2).

fib_when(N) when N == 1 -> 1;
fib_when(N) when N == 0 -> 0;
fib_when(N) when N > 1 -> fib_when(N - 1) + fib_when(N - 2).

tail_fib(N) -> tail_fib_helper(N, 0, 1).
tail_fib_helper(0, Acc1, _) -> Acc1;
tail_fib_helper(N, Acc1, Acc2) -> tail_fib_helper(N - 1, Acc1 + Acc2, Acc1).
